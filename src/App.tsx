import './App.css'
import { Main } from './components/Main'
import { Footer } from './components/Footer'
import { BrowserRouter } from 'react-router-dom';

function App() {

  return (
    <>
    {/* basename="/portfolio/" */}
      <BrowserRouter basename={import.meta.env.VITE_BASE_NAME}>
        <Main/>
        <Footer/>
      </BrowserRouter>
    </>
  )
}

export default App
