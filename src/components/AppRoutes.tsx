import { Routes, Route } from 'react-router-dom'
import { Home } from '../pages/Home'
import { Project } from '../pages/Project'

export const AppRoutes = () => {
    return (
        <Routes>
            <Route index element={<Home/>}></Route>
            <Route path='/*' element={<Project/>}></Route>
        </Routes>
    )
}
