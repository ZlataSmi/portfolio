export const Footer = () => {

return (
   <div className="footer">
      <h3>Контакты</h3>
      <div className="contacts__wrapper">
         <div className="contacts__col">
            <span className="contacts">Telegram <a className="contacts__link" href="https://t.me/ZlaSmi">@ZlaSmi</a></span>
            <span className="contacts">email <a className="contacts__link" href="mailto:smzlata0496@gmail.com">smzlata0496@gmail.com</a></span>
            <span className="contacts">tel. <a className="contacts__link" href="tel:+375296220199">+375 29 622 01 99</a></span>
         </div>
         <div className="contacts__col">
            <a className="contacts__link" href="https://www.linkedin.com/in/zlatasmirnova/">LinkedIn</a>
            <a className="contacts__link" href="https://gitlab.com/ZlataSmi">GitLab</a>
         </div>
      </div>
   </div>
 )
}