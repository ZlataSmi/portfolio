import { AppRoutes } from "./AppRoutes"

export const Main = () => {

    return (
        <div className="main">
            <div className="content">
                <AppRoutes/> 
            </div>
                      
        </div>
    )
}