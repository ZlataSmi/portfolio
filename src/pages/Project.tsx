import { Carousel, Image } from "antd"
import { Link } from "react-router-dom"
import * as data from '../db/projects.json'
import {handleClichPortfolioFake} from "./Home"

export const Project = () => {

    const fillPojects = () => {
        const html : React.JSX.Element[] = [];
        const currentPath = window.location.pathname;
        const project = data.projects.find((project) => project.id == currentPath.split('/').pop()); 
        html.push (
            <>{project && 
            <div className={`project ${project.id}`} key={`project_${project.id}`}>
                {project.name && <h3>{project.name}</h3>}    
                    
                {project.description_long && project.description_long.split('\n').map((str, index) => 
                    <p key={`p_${index}`}>{str}</p>
                )}

                {project.images  &&
                <Carousel autoplay>
                    {project.name && project.images.map((image, index) => (
                        <div key={`img_carousel_${index}`}>
                            <div className="project__carousel__wrap">
                                <Image className="project__carousel__img" src={`./img/${image}`} alt={project.name} />
                            </div>
                        </div>
                    ))
                    }
                </Carousel >
                }

                <div key={`project__links_${project.id}`} className="project__links__wrap">
                    {project.links ? Object.keys(project.links).map((linkKey, index) => (
                        <> {project.id == "portfolio" ? 
                            <>
                            <div key={`link_${project.id}_${index}`}  className="project__link portfolio__fake_link" onClick={handleClichPortfolioFake}>
                                <img className="project__link__icon" src="./img/open.png" alt="open" />
                                <span>{linkKey}</span>
                            </div>
                            <img key={`img_${project.id}`} id="portfolio__fake__message" hidden src="./img/you_are_here.png" alt="Вы уже здесь" />
                            </>
                        :                       
                            <Link key={`link_${project.id}_${index}`}  className="project__link" to={project.links[linkKey as keyof typeof project.links]!} target="_blank">
                                <img className="project__link__icon" src="./img/open.png" alt="open" />
                                <span>{linkKey}</span>
                            </Link>
                        }
                        </>
                    )) : <></>}
                    {project.source_code &&
                    <Link className="project__link" to={project.source_code} target="_blank">
                        <img className="project__link__icon" src="./img/gitlab2.webp" alt="gitLab" />
                        <span>Исходный код</span>
                    </Link>}
                </div>
                <div className="technology_tags__wrapper">
                    {project.technologies && project.technologies.map((technology, index) => (
                            <span key={`technology_${project.id}_${index}`} className="technology">{technology}</span>
                    ))}
                </div>
            </div>
            }</>
        )
        return html
    }

    return (
        <div className="project_page">
            <Link className="back__btn" to='/'>← Назад к проектам</Link>
            {fillPojects()}
            <Link className="back__btn" to='/'>← Назад к проектам</Link>
        </div>
    )
}