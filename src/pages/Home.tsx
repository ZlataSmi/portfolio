import { Link } from "react-router-dom"
import * as data from '../db/projects.json'

export const handleClichPortfolioFake = () => {
    const message = document.getElementById('portfolio__fake__message');
    if (message != null) {
        message.hidden = false;
        setTimeout(() => {
            message.hidden = true;
        }, 2000)
    }
}

export const Home = () => {

    const handleClichPortfolioFake = () => {
        const message = document.getElementById('portfolio__fake__message');
        if (message != null) {
            message.hidden = false;
            setTimeout(() => {
                message.hidden = true;
            }, 2000)
        }
    }

    const fillPojects = () => {
        const html : React.JSX.Element[] = []
        data.projects.map((project) => {
            html.push (
                <>{project.id &&
                <div className="project" key={`project_${project.id}`}>
                    {project.images && project.name && <img className="project__img" src={`./img/${project.images[0]}`} alt={project.name} />}
                    {project.name && <h3 className="project__name">{project.name}</h3>}    
                        
                    {project.description_short && <p>{project.description_short}</p>}

                    <div key={`project__links_${project.id}`} className="project__links__wrap">
                        {project.id && 
                        <Link className="project__link" to={`./${project.id}`}>
                            <img className="project__link__icon" src="./img/look.png" alt="open" />
                            <span>Подробнее</span>
                        </Link>}
                        {project.links ? Object.keys(project.links).map((linkKey, index) => (
                            <> {project.id == "portfolio" ? 
                                <>
                                <div key={`link_${project.id}_${index}`}  className="project__link portfolio__fake_link" onClick={handleClichPortfolioFake}>
                                    <img className="project__link__icon" src="./img/open.png" alt="open" />
                                    <span>{linkKey}</span>
                                </div>
                                <img key={`img_${project.id}`} id="portfolio__fake__message" hidden src="./img/you_are_here.png" alt="Вы уже здесь" />
                                </>
                            :                       
                                <Link key={`link_${project.id}_${index}`}  className="project__link" to={project.links[linkKey as keyof typeof project.links]!} target="_blank">
                                    <img className="project__link__icon" src="./img/open.png" alt="open" />
                                    <span>{linkKey}</span>
                                </Link>
                            }
                            </>
                        )) : <></>}
                        {project.source_code &&
                        <Link className="project__link" to={project.source_code} target="_blank">
                            <img className="project__link__icon" src="./img/gitlab2.webp" alt="gitLab" />
                            <span>Исходный код</span>
                        </Link>}
                    </div>
                    <div className="technology_tags__wrapper">
                        {project.technologies && project.technologies.map((technology, index) => (
                                <span key={`technology_${project.id}_${index}`} className="technology">{technology}</span>
                        ))}
                    </div>
                </div>}</>
            )
        })
        return html
    }

    return (
        <div className="home__page">
            <div className="hello__wrapper">
                <h2>Вместо вступления</h2>
                <span className="helo__items hello__header">Привет! Я Смирнова Злата, Front-end разработчик.</span>
                <span className="helo__items hello__text">Из множества возможных направлений я выбрала для себя веб-разработку и именно "front" часть, потому что предпочитаю "видеть глазами" результат своей работы. Так что давайте вместо тысячи слов "обо мне" сразу к проектам :) </span>
                <span className="helo__items hello__sub">К слову, данный сайт-портфолио написан с использованием: JavaScript, TypeScript, React, Vite, HTML/CSS</span>
            </div>

            <div className="projects">
                <h2>Мои проекты</h2>
                <div className="projects__wrapper">
                    {fillPojects()}
                </div>
            </div>

            <div className="about__wrapper">
                <h2 className="about__header">Обо мне</h2>
                <img className="photo" src="./img/portrait.jpeg" alt="my photo" />
                <p className="about">Пусть этот сайт — портфолио моих проектов, всё же стоит рассказать пару слов о себе (хотя едва ли сюда можно было попасть, минуя моё резюме и/или страничку на LinkedIn, где есть вся-вся информация).</p>
                <p className="about">Я Злата Смирнова, 28 лет. Родилась, живу и работаю в Минске. Училась в БГУИРе на специальности "Защита информации". По данному профилю работаю уже почти 7 лет.</p>
                <ul className="about"> На текущем месте работы занималась:
                    <li><span>Cопровождением процедуры сертификации средств защиты информации и их испытаниями</span></li>
                    <li><span>Созданием и ведением сайта компании. SEO-оптимизацией</span></li>
                    <li><span>Участвовала в разработке и развитии веб-интерфейсов информационных продуктов компании</span></li>
                </ul>
                <p className="about">Начинала интересоваться веб-разработкой ещё давно и с мелочей, вроде &#171;помочь знакомым с сайтом&#187; или использования старых интернет-блогов, где для любого мало-мальски красивого поста нужно неплохо разбираться в вёрстке и HTML/CSS. После того, как на работе поставили задачу сделать сайт компании, начала самостоятельно изучать веб-разработку. Позже углубила и систематизировала знания на курсах</p>
                <p className="about">В свободное время увлекаюсь созданием декораций для фанатского стенда на фестивалях фантастики, что даёт дополнительный опыт организации, планирования и работы в команде в сжатые сроки и с поиском неожиданных путей решения внезапно возникающих проблем. В том числе помогала с одним из сайтов фестиваля, делала на него веб-квест, а так же две браузерные игры для постетилей стенда. (более 5 тыс игроков за два дня).  </p>
            </div>
        </div>
    )
}